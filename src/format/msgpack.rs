// Copyright 2015 Till Höppner
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::io::{ BufRead, Write };
use std::iter::Iterator;

use event::Event;
use context::Context;
use format::{ Encode, Decode };

use rustc_serialize::{ Encodable, Decodable };
use msgpack::{ Encoder, Decoder };
use rmp::decode::ReadError;

pub struct Msgpack;

pub struct Iter<'a> {
    input: &'a mut BufRead
}

impl<'a> Iterator for Iter<'a> {
    type Item = ::Result<Event<'a>>;
    fn next(&mut self) -> Option<::Result<Event<'a>>> {
        use msgpack::decode;
        match Event::decode(&mut Decoder::new(&mut self.input)) {
            Ok(e) => Some(Ok(e)),
            Err(decode::Error::InvalidMarkerRead(ReadError::UnexpectedEOF)) => None,
            Err(e) => Some(Err(::IlcError::MsgpackDecode(e)))
        }
    }
}

impl Encode for Msgpack {
    fn encode<'a>(&'a self, _context: &'a Context, output: &'a mut Write, event: &'a Event) -> ::Result<()> {
        event.encode(&mut Encoder::new(output))
            .map_err(|e| ::IlcError::MsgpackEncode(e))
    }
}

impl Decode for Msgpack {
    fn decode<'a>(&'a mut self, _context: &'a Context, input: &'a mut BufRead)
        -> Box<Iterator<Item = ::Result<Event<'a>>> + 'a> {
        Box::new(Iter { input: input })
    }
}
